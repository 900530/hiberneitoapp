package itakademija.java2015.jpa.assigment1.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AuthorTag.class)
public abstract class AuthorTag_ {

	public static volatile SingularAttribute<AuthorTag, Long> id;
	public static volatile SingularAttribute<AuthorTag, String> tag;
	public static volatile SingularAttribute<AuthorTag, Date> createdOn;

}

