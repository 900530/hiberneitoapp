package itakademija.java2015.ui.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.jpa.assigment1.entities.Author;
import itakademija.java2015.jpa.assigment1.entities.AuthorTag;
import itakademija.java2015.jpa.assigment1.entities.repositories.jpa.AuthorDaoImpl;

public class AuthorsListPageBean {
	private static final String NAV_SHOW_AUTHORS_BY_TAG = "show-tags";

	static final Logger log = LoggerFactory.getLogger(AuthorsListPageBean.class);

	public static final String NAV_SHOW_ADD_BOOK = "show-add-book";
	public static final String NAV_LIST_AUTHORS = "list-authors";


	public static class AuthorsListPageData implements Serializable {

		private static final long serialVersionUID = -7847613490719023414L;

		@Valid
		
		private Author newAuthor;


		@Valid
		private Author currentAuthor;

		private String tags;
		
		private String searchTag;
		
		private List<Author> foundAuthors;


		public void init() {
			newAuthor = new Author();
			foundAuthors = new ArrayList<>();
		}

		public String getSearchTag() {
			return searchTag;
		}

		public void setSearchTag(String searchTag) {
			this.searchTag = searchTag;
		}

		public List<Author> getFoundAuthors() {
			return foundAuthors;
		}

		public void setFoundAuthors(List<Author> foundAuthors) {
			this.foundAuthors = foundAuthors;
		}

		public String getTags() {
			return tags;
		}

		public void setTags(String tags) {
			this.tags = tags;
		}

		public Author getCurrentAuthor() {
			return currentAuthor;
		}

		public void setCurrentAuthor(Author currentAuthor) {
			this.currentAuthor = currentAuthor;
		}

		public Author getNewAuthor() {
			return newAuthor;
		}

		public void setNewAuthor(Author newAuthor) {
			this.newAuthor = newAuthor;
		}
	} 


	private AuthorsListPageData data;

	private AuthorDaoImpl authorRepo;

	public String addNew() {
		String tagsString = data.getTags();
		if (tagsString != null) {
			List<AuthorTag> tagList = convertTagStringToTagList(tagsString);

			data.newAuthor.setTags(tagList);
		}
		authorRepo.save(data.newAuthor);
		data.newAuthor = new Author();
		data.tags = "";
		return NAV_LIST_AUTHORS;
	}
	public String convertTagListToTagString(List<AuthorTag> tags) {
		String ret = "";
		for (AuthorTag tag: tags) {
			ret += tag.getTag() + " ";
		}
		return ret;
	}
	private List<AuthorTag> convertTagStringToTagList(String tagsString) {
		log.debug("Converting string[{}]", tagsString);
		List<AuthorTag> tagList = new ArrayList<>();

		if (tagsString != null) {
	
			String[] res = tagsString.split("\\s+");
			log.debug("Split size: {}", res.length);
			List<String> tagsArray = Arrays.asList(res);
			log.debug("List size:{}", tagsArray.size());
			for (String tagStr : tagsArray) {
				AuthorTag t = null;
				t = authorRepo.findTag(tagStr);
				if (t == null)
				{
					log.debug("New tag: {}", tagStr);
					t = new AuthorTag();
					t.setCreatedOn(new Date());
				}
				else
					log.debug("Existing tag: {}", tagStr);
				log.debug("Adding single tag: [{}]", tagStr);
				t.setTag(tagStr);
				tagList.add(t);
			}
		}
		return tagList;
	}

	public String deleteSelected(Author author) {
		if (author == null)
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Author being deleted is null?"));
		else {
			if (author.getName().equalsIgnoreCase("Jonas"))
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "You are deleting Jonas!"));
			authorRepo.delete(author);
		}
		return NAV_LIST_AUTHORS;
	}


	public String showAddBookPage(Author a) {
		log.debug("Will store selected author for later access in Add new Book form: {}", a);
		data.currentAuthor = a;
		return NAV_SHOW_ADD_BOOK;
	}

	public String searchTags() {
		//save to session
		data.foundAuthors = authorRepo.findAllAuthorsByTag(data.getSearchTag());
		return NAV_SHOW_AUTHORS_BY_TAG;
	}
	public AuthorsListPageData getData() {
		return data;
	}

	public void setData(AuthorsListPageData data) {
		this.data = data;
	}

	public AuthorDaoImpl getAuthorRepo() {
		return authorRepo;
	}

	public void setAuthorRepo(AuthorDaoImpl authorRepo) {
		this.authorRepo = authorRepo;

	}

	public List<Author> getAuthorList() {
		return authorRepo.findAll();
	}
}
