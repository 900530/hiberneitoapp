package itakademija.java2015.ui.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.jpa.assigment1.entities.Author;
import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.assigment1.entities.repositories.jpa.AuthorDaoImpl;
import itakademija.java2015.jpa.assigment1.entities.repositories.jpa.BookRepositoryJPA;

public class AddNewBookPageBean {
	static final Logger log = LoggerFactory.getLogger(AddNewBookPageBean.class);

	private AuthorDaoImpl authorRepo;
	private BookRepositoryJPA bookRepo;

	private AuthorsListPageBean authorsListPageBean;
	
	private Book newBook;
	
	public void init() {
		newBook = new Book();
	}
	
	public String addNew() {

		Author author = authorsListPageBean.getData().getCurrentAuthor();
		log.debug("Before saving book, got author: {}", author);
		log.debug("New book data: {}", newBook);
		newBook.setAuthor(author);
		author.addBook(newBook);
		authorRepo.save(author);
		
		return AuthorsListPageBean.NAV_LIST_AUTHORS;
	}
	
	public AuthorsListPageBean getAuthorsListPageBean() {
		return authorsListPageBean;
	}
	public void setAuthorsListPageBean(AuthorsListPageBean authorsListPageBean) {
		this.authorsListPageBean = authorsListPageBean;
	}
	public AuthorDaoImpl getAuthorRepo() {
		return authorRepo;
	}
	public void setAuthorRepo(AuthorDaoImpl authorRepo) {
		this.authorRepo = authorRepo;
	}
	public BookRepositoryJPA getBookRepo() {
		return bookRepo;
	}
	public void setBookRepo(BookRepositoryJPA bookRepo) {
		this.bookRepo = bookRepo;
	}
	public Book getNewBook() {
		return newBook;
	}
	public void setNewBook(Book newBook) {
		this.newBook = newBook;
	}
}
