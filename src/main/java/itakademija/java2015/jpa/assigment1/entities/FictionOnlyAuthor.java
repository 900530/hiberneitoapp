package itakademija.java2015.jpa.assigment1.entities;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Table;

@Table(name="AUTHOR")
@DiscriminatorValue("F")
public class FictionOnlyAuthor extends Author {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2161590463566713623L;
	private String nickNameInFictionCommunity;

	public String getNickNameInFictionCommunity() {
		return nickNameInFictionCommunity;
	}

	public void setNickNameInFictionCommunity(String nickNameInFictionCommunity) {
		this.nickNameInFictionCommunity = nickNameInFictionCommunity;
	}
	
}
