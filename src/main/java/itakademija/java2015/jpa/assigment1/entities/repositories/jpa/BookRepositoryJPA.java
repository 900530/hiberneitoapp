package itakademija.java2015.jpa.assigment1.entities.repositories.jpa;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import itakademija.java2015.jpa.assigment1.entities.Author;
import itakademija.java2015.jpa.assigment1.entities.AuthorTag;
import itakademija.java2015.jpa.assigment1.entities.AuthorTag_;
import itakademija.java2015.jpa.assigment1.entities.Author_;
import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.assigment1.entities.Book_;


public class BookRepositoryJPA {

	private EntityManagerFactory entityManagerFactory;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}


	public void insertOrUpdate(Book book) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			boolean merged = false;
			for (AuthorTag t : book.getAuthor().getTags()) {
				if (!entityManager.contains(t) && t.getId() != null) {
					entityManager.merge(t);
					merged = true;
				} else
					entityManager.persist(t);
			}
			if (merged)
				entityManager.merge(book);
			else
				entityManager.persist(book);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}


	public void delete(Book book) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(book);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}


	public void deleteById(Long bookId) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			Book book = entityManager.find(Book.class, bookId);
			if (book != null)
				entityManager.remove(book);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}


	public List<Book> findByTitleFragment(String titleFragment) {
		EntityManager entityManager = getEntityManager();
		TypedQuery<Book> q;
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Book> cq = cb.createQuery(Book.class);
			Root<Book> bookRoot = cq.from(Book.class);
			cq.select(bookRoot);
			/**
			 * Construct SQL in typesafe way should result in
			 * "SELECT ... FROM book WHERE lower(title) like lower('%blahblah%')"
			 */
			cq.where(cb.like(cb.lower(bookRoot.get(Book_.title)), "%" + titleFragment.toLowerCase() + "%"));
			q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}


	public Long countAllBooks() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
			countQuery.select(cb.count(countQuery.from(Book.class)));
			TypedQuery<Long> q = entityManager.createQuery(countQuery);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}

	public List<Book> findBooks(String nameOrLastname, Integer year, String titleFragment, String tag) {

		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Book> cq = cb.createQuery(Book.class);
			Root<Author> authorRoot = cq.from(Author.class);
			ListJoin<Author, Book> bookJoin = authorRoot.join(Author_.books);
			ListJoin<Author, AuthorTag> tagJoin = authorRoot.join(Author_.tags, JoinType.LEFT);
			cq.select(bookJoin);
			List<Predicate> predicates = new ArrayList<>();
			Predicate predicate = null;
			if (nameOrLastname != null) {
				predicate = cb.or(cb.like(cb.lower(authorRoot.get(Author_.name)),
						"%" + nameOrLastname.toLowerCase() + "%"),cb.like(cb.lower(authorRoot.get(Author_.lastname)),
						"%" + nameOrLastname.toLowerCase() + "%"));
				predicates.add(predicate);
			}
			if (year != null) {
				Date fromDate = buildFromDate(year);
				Date tillDate = buildTillDate(year);

				predicate =
				cb.between(bookJoin.get(Book_.releaseDate), fromDate, tillDate);
				predicates.add(predicate);
			}
			if (titleFragment != null) {
				predicate = cb.like(bookJoin.get(Book_.title), likeClause(titleFragment));
				predicates.add(predicate);
			}
			if (tag != null) {
				predicate = cb.equal(tagJoin.get(AuthorTag_.tag),tag);
				predicates.add(predicate);
			}

			Predicate[] predicateArray = predicates.toArray(new Predicate[] {});
			cq.where(cb.and(predicateArray));
			TypedQuery<Book> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	public List<Book> findBooks2(String nameOrLastname, Integer year, String titleFragment, String tag) {
		String jpql = "SELECT b FROM Author a JOIN a.books b LEFT JOIN a.tags t ";
		String whereClause = "";
		if (nameOrLastname != null) {
			if (!whereClause.isEmpty())
				whereClause += " AND ";
			whereClause += "(lower(a.name) like lower(:name) OR lower(a.lastname) like lower(:name2))";
		}
		
		if (year != null) {
			if (!whereClause.isEmpty())
				whereClause += " AND ";
			whereClause += "(b.releaseDate BETWEEN :since and :till)";
		}
		
		if (titleFragment != null)
		{
			if (!whereClause.isEmpty())
				whereClause += " AND ";
			whereClause += "(b.title LIKE :title)";
		}
		
		if (tag != null) 
		{
			if (!whereClause.isEmpty())
				whereClause += " AND ";
			whereClause += "t.tag = :tag)";
		}
		
		if (!whereClause.isEmpty())
			whereClause = "WHERE  " + whereClause;
		jpql += whereClause;
		
		EntityManager entityManager = getEntityManager();
		try {
			TypedQuery<Book> q = entityManager.createQuery(jpql, Book.class);
			if (titleFragment != null)
				q.setParameter("title", likeClause(titleFragment));
			if (nameOrLastname != null) {
				q.setParameter("name", likeClause(nameOrLastname));
				q.setParameter("name2", likeClause(nameOrLastname));
			}
			
			if (year != null) {
				Date fromDate = buildFromDate(year);

				Date tillDate = buildTillDate(year);
				q.setParameter("since", fromDate);
				q.setParameter("till", tillDate);
			}
			
			if (tag != null) {
				q.setParameter("tag", tag);
			}
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	private Date buildTillDate(Integer year) {
		Instant till = LocalDateTime.of(year + 1, 1, 1, 0, 0).toInstant(ZoneOffset.UTC);
		Date tillDate = Date.from(till);
		return tillDate;
	}

	private Date buildFromDate(Integer year) {
		Instant since = LocalDateTime.of(year, 1, 1, 0, 0).toInstant(ZoneOffset.UTC);
		Date fromDate = Date.from(since);
		return fromDate;
	}

	private String likeClause(String field) {
		return "%" + field + "%";
	}

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
}
