package itakademija.java2015.jpa.assigment1.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Book implements Serializable {
	private static final long serialVersionUID = -2592264505437536988L;
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	@NotBlank
	private String title;
	@Temporal(TemporalType.DATE)
	private Date releaseDate;
	/**
	 * Edition of the book; For example: "second", "2nd", "anniversary", ..
	 */
	private String edition;
	@JoinColumn(name = "author_id")
	@ManyToOne(optional = true, cascade = { CascadeType.ALL })
	private Author author;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
		author.addBook(this);
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + ", releaseDate=" + releaseDate + ", edition=" + edition
				+ ", author=" + ((author == null) ? null : author.getName()) + "]" + super.toString();
	}
}
