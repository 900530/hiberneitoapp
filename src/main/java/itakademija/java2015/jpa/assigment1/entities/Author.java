package itakademija.java2015.jpa.assigment1.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(indexes = @Index(columnList = "name ASC, lastname DESC", name = "AUTH_NAME_IDX", unique=false) , name = "AUTHOR")
@DiscriminatorColumn(discriminatorType = DiscriminatorType.CHAR, length = 1, name = "AUTHOR_TYPE")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("G")
public class Author implements Serializable {

	private static final long serialVersionUID = 5089035398648855772L;
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	@NotBlank

	private String name;
	private String lastname;

	@OneToMany(mappedBy = "author", fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	private List<Book> books;

	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
	private List<AuthorTag> tags;

	public List<AuthorTag> getTags() {
		return tags;
	}

	public void setTags(List<AuthorTag> tags) {
		this.tags = tags;
	}

	// @OneToOne(cascade=CascadeType.ALL)
	@Embedded
	private Address address;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void addBook(Book b) {
		if (getBooks() == null)
			setBooks(new ArrayList<>());
		if (!getBooks().contains(b))
			getBooks().add(b);
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String surname) {
		this.lastname = surname;
	}


	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", lastname=" + lastname + ", books=["
				+ ((books == null) ? books : books.size()) + "]" + ", address=" + address + "]" + super.toString();
	}

}
